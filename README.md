# postcss-g-index
postcss library for z-index handling

[![Beneath the sun logo](https://raw.githubusercontent.com/pixelass/beneath-the-sun/master/beneath-the-sun.png)](https://github.com/pixelass/beneath-the-sun)

## g-index

a small collection of functions and mixins to handle your elements’ `z-index`. 
`g-index` uses "G"eographical names to make it very clear where the element will be placed. 

## npm

```shell
npm install --save-dev postcss-g-index
```

## bower

```bower
bower install postcss-g-index
```

## Demo

> The demo uses the SCSS version
> [Beneath the sun](https://github.com/pixelass/beneath-the-sun)

[Codepen demo](http://codepen.io/pixelass/pen/yJGjYr)

## Names

```scss
'underground' 'sea'   'land' 'hill'
 = -3          = 0     = 3     = 6
'mountain'    'cloud' 'moon' 'sun'
 = 9           = 12    = 15   = 18
```
## Usage

```js
postcss().use(require('postcss-g-index'))
  .process(/* ... */)
  .then(/* ... */)
```
### input

```scss
.tooltip {
  z-index: g-index('land');
}
.dialog {
  z-index: g-index('mountain');
}
.dialog-backdrop {
  z-index: g-below('mountain');
}
```

### output

```css
.tooltip {
  z-index: 0;
}
.dialog {
  z-index: 9;
}
.dialog-backdrop {
  z-index: 8;
}
```
## Custom levels

```js
const gIndex = require('postcss-g-index')

postcss().use(gIndex({
  levels: {
    'down-there': -1,
    'here': 0,
    'above-us': 1,
    'out-of-reach': 5,
    'way-up-in-the-sky': 100
  }
}))
  .process(/* ... */)
  .then(/* ... */)
```



