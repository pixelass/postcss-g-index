const postcss = require('postcss')

const q = '[\"\']'
const patterns = [
  'g-below\\((.*)\\)',
  'g-index\\((.*)\\)',
  'g-above\\((.*)\\)'
]

/**
 * remove quotes around string
 * @param  {String} str - raw string
 * @param  {String} pattern - capture pattern
 * @return {String} - returns the cleaned string
 */
const removeOuterQuotes = (str, pattern) => {
  const re = new RegExp(`${q}(${pattern})${q}`)
  const match = str.match(re)
  return match && match[1] || str
}

/**
 * processor around gIndex
 * @param  {String} str - raw value
 * @param  {String} pattern - capture pattern
 * @param  {Function} transform process method of g-index
 * @param  {Number} n - move direction [-1,0,1]
 * @return {String} returns the processed string
 */
const processor = (str, pattern, transform, n) => {
  const matches = str.match(new RegExp(pattern))
  if (!matches) {
    return str
  }
  const valParts = matches[1]
    .replace(new RegExp(`${q}`, 'g'), '')
    .split(',')
  const move = Math.abs(n) ? n : Number(valParts[1] || 0)
  const processed = transform(valParts[0].trim(), move)
  return str.replace(removeOuterQuotes(str,pattern), processed)
}

/**
 * @constructor
 */
class GIndex {
  /**
   * g-index transformer
   * @param  {Object} [levels] - custom level map
   * @return {Function} returns the process method
   */
  constructor(levels) {
    // default map
    const defaults = {
      underground: -3,
      sea: 0,
      land: 3,
      hill: 6,
      mountain: 9,
      cloud: 12,
      moon: 15,
      sun: 18
    }
    this.levels = levels || defaults
    return this.process.bind(this)
  }

  /**
   * process method
   * @param  {String} level - name of level
   * @param  {Number} move - number of steps to move [-1,0,1]
   * @return {Number|String} returns the computed value
   */
  process(level, move) {
    const limitMove = 1
    // limit moves to limitMove
    if (Math.abs(move) > limitMove) {
      throw new Error(`You can only move ${limitMove} level 
                       but attempted to move ${Math.abs(move)} levels.`)
    } else {
      // only proceed for valid keys
      if (level in this.levels) {
        // calculate the `z-index` from the map value and the `move` value
        return this.levels[level] + move
      } else {
        // invalid key return an error
        throw new Error(`The map levels does not contain a key called ${level}.`)
      }
    }
  }
}

/**
 * define postcss plugin
 * @param  {String} name - name of plugin
 * @param  {Object} options - options to use with plugin
 * @return {Function} retuns the postcss method
 */
const plugin = postcss.plugin('postcss-g-index', (options = {}) => style => {
  style.walkDecls(r => {
    if ('value' in r) {
      patterns.forEach((pattern, i) => {
        r.value = processor(r.value, pattern, new GIndex(options.levels), (i - 1))
      })
    }
  })
})

export default plugin
