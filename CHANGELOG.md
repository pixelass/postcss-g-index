# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.1"></a>
## [0.1.1](https://github.com/pixelass/postcss-g-index/compare/v0.1.0...v0.1.1) (2016-08-12)


### Bug Fixes

* **string:** better sting handling ([ac46e54](https://github.com/pixelass/postcss-g-index/commit/ac46e54))



<a name="0.1.0"></a>
# [0.1.0](https://github.com/pixelass/postcss-g-index/compare/v0.0.2...v0.1.0) (2016-08-11)


### Features

* **options:** custom levels ([b4c9f7e](https://github.com/pixelass/postcss-g-index/commit/b4c9f7e))



<a name="0.0.2"></a>
## [0.0.2](https://github.com/pixelass/postcss-g-index/compare/v0.0.1...v0.0.2) (2016-08-11)


### Bug Fixes

* **strings:** allow different quotes ([4fe9f8c](https://github.com/pixelass/postcss-g-index/commit/4fe9f8c))



<a name="0.0.1"></a>
## 0.0.1 (2016-08-11)
