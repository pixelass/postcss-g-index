const fs = require('fs')
const postcss = require('postcss')
const colors = require('colors')
const gIndex = require('../')

const dir = './test'


const filePost = `${dir}/postcss-test_2.css`
const expectedPost = `${dir}/postcss-expected_2.css`
const outFilePost = `${dir}/postcss-result_2.css`

function xPect(testType, xpct, rslt) {
  fs.readFile(xpct,'utf-8',(err, data)=> {
    if (err) {
      throw err
    } else {
      const xpctd = data
      if (typeof rslt === 'string') {
        fs.readFile(rslt,'utf-8',(err, data)=> {
          if (err) {
            throw err
          } else {
            if (xpctd === data) {
              console.log(`👍  ${testType} test passed`.green)
            } else {
              console.log(`👎  ${testType} test failed`.red)
              throw new Error('Test failed.')
            }
          }
        })
      } else if (typeof rslt === 'object') {
        if ('css' in rslt) {
          if (xpctd === rslt.css) {
            console.log(`👍  ${testType} test passed`.green)
          } else {
            console.log(`👎  ${testType} test failed`.red)
            throw new Error('Test failed.')
          }
        }
      }
    }
  })
}

fs.readFile(filePost, 'utf-8', (err,result)=> {
  if (err) {
    throw err
  } else {
  postcss()
    .use(gIndex({
      levels: {
        one: 1,
        two: 2,
        three: 3,
        four: 4,
        five: 5
      }
    }))
    .process(result, { from: filePost, to: outFilePost })
    .then((result)=> {
      fs.writeFile(outFilePost, result.css)
      xPect('postcss', expectedPost, result)
    }).catch(err => {
      throw err
    })
  }
})
